import sys

key = None
encrypt = True


def f(_r0, _r1, _round):
    _k = []
    global encrypt
    if encrypt:
        # print("Encrypt")
        for i in range(12):
            _k.append(K(4*_round+(i % 4)))
    else:
        # print("Decrypt")
        for i in range(12):
            _k.insert(0, K(4*_round+(3-(i % 4))))
    t0 = g(_r0, _k[0], _k[1], _k[2], _k[3], _round)
    t1 = g(_r1, _k[4], _k[5], _k[6], _k[7], _round)
    _f0 = format(add16(add16(t0, multiply16(2, t1)), concat(_k[8], _k[9])))
    _f1 = format(add16(add16(multiply16(2, t0), t1), concat(_k[10], _k[11])))
    return _f0, _f1


def g(_r0, k0, k1, k2, k3, _round):
    g1 = left8(_r0)
    g2 = right8(_r0)
    g3 = xor8(f_table_lookup(xor8(g2[:], k0)), g1[:])
    g4 = xor8(f_table_lookup(xor8(g3[:], k1)), g2[:])
    g5 = xor8(f_table_lookup(xor8(g4[:], k2)), g3[:])
    g6 = xor8(f_table_lookup(xor8(g5[:], k3)), g4[:])
    return concat(g5, g6)


def K(_x):
    global key
    if encrypt:
        # print("Encrypt")
        key = rotate(key, 1)
        k_list = list(get_keys8(key))
    else:
        # print("Decrypt")
        k_list = list(get_keys8(key))
        key = rotate(key, -1)
    return k_list[_x % 8]


def get_words(_block):
    if encrypt:
        for i in range(0, 8, 2):
            word = _block[i:i+2]
            yield concat(format(ord(word[0]), '08b'), format(ord(word[1]), '08b'))
    else:
        for i in range(0, 16, 4):
            word = _block[i:i+4]
            hex0 = format(int(word[0], 16), '04b')
            hex1 = format(int(word[1], 16), '04b')
            hex2 = format(int(word[2], 16), '04b')
            hex3 = format(int(word[3], 16), '04b')
            yield hex0 + hex1 + hex2 + hex3


def get_keys8(_key):
    for i in range(0, 64, 8):
        yield _key[i:i+8:1]


def get_keys16(_key):
    for i in range(0, 64, 16):
        yield _key[i:i+16:1]


def left4(_key):
    return _key[:4]


def right4(_key):
    return _key[4:]


def left8(_key):
    return _key[:8]


def right8(_key):
    return _key[8:]


def concat(bin1, bin2):
    return format(int(bin1, 2) << len(bin2) | int(bin2, 2), '016b')


# Rotate n bits in the left or right direction
# For left rotations n will be positive
# For right rotations n will be negative
def rotate(_key, n):
    return _key[n:] + _key[:n]


def xor8(bin1, bin2):
    return format(int(bin1, 2) ^ int(bin2, 2), '08b')


def xor16(bin1, bin2):
    return format(int(bin1, 2) ^ int(bin2, 2), '016b')


def add16(bin1, bin2):
    return truncate(format(int(bin1, 2) + int(bin2, 2), '016b'))


def multiply16(n, bin1):
    return truncate(format(2 * int(bin1, 2), '016b'))


def truncate(_bin):
    if len(_bin) == 17:
        return _bin[1:]
    else:
        return _bin


def f_table_lookup(_bin):
    return format(f_table[(int(left4(_bin), 2)*16) + int(right4(_bin), 2)], '08b')


f_table = [0xa3, 0xd7, 0x09, 0x83, 0xf8, 0x48, 0xf6, 0xf4, 0xb3, 0x21, 0x15, 0x78, 0x99, 0xb1, 0xaf, 0xf9,
           0xe7, 0x2d, 0x4d, 0x8a, 0xce, 0x4c, 0xca, 0x2e, 0x52, 0x95, 0xd9, 0x1e, 0x4e, 0x38, 0x44, 0x28,
           0x0a, 0xdf, 0x02, 0xa0, 0x17, 0xf1, 0x60, 0x68, 0x12, 0xb7, 0x7a, 0xc3, 0xe9, 0xfa, 0x3d, 0x53,
           0x96, 0x84, 0x6b, 0xba, 0xf2, 0x63, 0x9a, 0x19, 0x7c, 0xae, 0xe5, 0xf5, 0xf7, 0x16, 0x6a, 0xa2,
           0x39, 0xb6, 0x7b, 0x0f, 0xc1, 0x93, 0x81, 0x1b, 0xee, 0xb4, 0x1a, 0xea, 0xd0, 0x91, 0x2f, 0xb8,
           0x55, 0xb9, 0xda, 0x85, 0x3f, 0x41, 0xbf, 0xe0, 0x5a, 0x58, 0x80, 0x5f, 0x66, 0x0b, 0xd8, 0x90,
           0x35, 0xd5, 0xc0, 0xa7, 0x33, 0x06, 0x65, 0x69, 0x45, 0x00, 0x94, 0x56, 0x6d, 0x98, 0x9b, 0x76,
           0x97, 0xfc, 0xb2, 0xc2, 0xb0, 0xfe, 0xdb, 0x20, 0xe1, 0xeb, 0xd6, 0xe4, 0xdd, 0x47, 0x4a, 0x1d,
           0x42, 0xed, 0x9e, 0x6e, 0x49, 0x3c, 0xcd, 0x43, 0x27, 0xd2, 0x07, 0xd4, 0xde, 0xc7, 0x67, 0x18,
           0x89, 0xcb, 0x30, 0x1f, 0x8d, 0xc6, 0x8f, 0xaa, 0xc8, 0x74, 0xdc, 0xc9, 0x5d, 0x5c, 0x31, 0xa4,
           0x70, 0x88, 0x61, 0x2c, 0x9f, 0x0d, 0x2b, 0x87, 0x50, 0x82, 0x54, 0x64, 0x26, 0x7d, 0x03, 0x40,
           0x34, 0x4b, 0x1c, 0x73, 0xd1, 0xc4, 0xfd, 0x3b, 0xcc, 0xfb, 0x7f, 0xab, 0xe6, 0x3e, 0x5b, 0xa5,
           0xad, 0x04, 0x23, 0x9c, 0x14, 0x51, 0x22, 0xf0, 0x29, 0x79, 0x71, 0x7e, 0xff, 0x8c, 0x0e, 0xe2,
           0x0c, 0xef, 0xbc, 0x72, 0x75, 0x6f, 0x37, 0xa1, 0xec, 0xd3, 0x8e, 0x62, 0x8b, 0x86, 0x10, 0xe8,
           0x08, 0x77, 0x11, 0xbe, 0x92, 0x4f, 0x24, 0xc5, 0x32, 0x36, 0x9d, 0xcf, 0xf3, 0xa6, 0xbb, 0xac,
           0x5e, 0x6c, 0xa9, 0x13, 0x57, 0x25, 0xb5, 0xe3, 0xbd, 0xa8, 0x3a, 0x01, 0x05, 0x59, 0x2a, 0x46]

if __name__ == "__main__":
    # Handle program input
    if len(sys.argv) > 1:
        if sys.argv[1] == '-decrypt':
            encrypt = False

    # Load in key
    key_file_filename = "key.txt"
    key_file = open(key_file_filename)
    hex_key = key_file.read(16)
    key = ''.join(format(int(x, 16), '04b') for x in hex_key)
    k = list(get_keys16(key))

    # Load file
    if encrypt:
        filename = "plaintext.txt"
    else:
        filename = "cyphertext.txt"
    file = open(filename, "r")

    # Ready output file
    if encrypt:
        output_filename = "cyphertext.txt"
    else:
        output_filename = "plaintextcopy.txt"
    out_file = open(output_filename, "w")

    # While file not empty read and process
    # For encryption read in 8 characters (8 bytes ascii)
    # For decryption read in 16 characters (8 bytes hex)
    if encrypt:
        block = file.read(8)
    else:
        block = file.read(16)

    # Loop while data to process
    while len(block) != 0:

        # Pad buffer if necessary
        if len(block) < 8:
            block = '\0' * (8 - len(block)) + block

        # Break into four 16 bit binary words
        w = list(get_words(block))

        # XOR words with keys
        r = [xor16(w[0], k[0]), xor16(w[1], k[1]), xor16(w[2], k[2]), xor16(w[3], k[3])]

        # Apply encryption round number of times
        if encrypt:
            # print("Encrypt")
            for _round in range(16):
                r0 = r[0][:]
                r1 = r[1][:]
                f0, f1 = f(r[0], r[1], _round)
                r[0] = rotate(xor16(f0, r[2]), -1)
                r[1] = xor16(f1, rotate(r[3], 1))
                r[2] = r0
                r[3] = r1
        else:
            # print("Decrypt")
            for _round in range(15, -1, -1):
                r0 = r[0][:]
                r1 = r[1][:]
                f0, f1 = f(r[0], r[1], _round)
                r[0] = xor16(f0, rotate(r[2], 1))
                r[1] = rotate(xor16(f1, r[3]), -1)
                r[2] = r0
                r[3] = r1

        # Undo the last swap
        y0 = r[2]
        y1 = r[3]
        y2 = r[0]
        y3 = r[1]

        # Output Whitening step only done once when _round is equal to 15
        c0 = xor16(y0, k[0])
        c1 = xor16(y1, k[1])
        c2 = xor16(y2, k[2])
        c3 = xor16(y3, k[3])

        # Write processed data to file
        dat = []
        if encrypt:
            dat.append(format(int(c0, 2), '04x'))
            dat.append(format(int(c1, 2), '04x'))
            dat.append(format(int(c2, 2), '04x'))
            dat.append(format(int(c3, 2), '04x'))
        else:
            ch = [chr(int(left8(c0), 2)), chr(int(right8(c0), 2)), chr(int(left8(c1), 2)), chr(int(right8(c1), 2)),
                  chr(int(left8(c2), 2)), chr(int(right8(c2), 2)), chr(int(left8(c3), 2)), chr(int(right8(c3), 2))]

            # Cleanse the padded data
            for i in range(len(ch)):
                if ch[i] == '\0':
                    ch[i] = ''

            # Concatenate characters into groups of two
            dat.append(ch[0] + ch[1])
            dat.append(ch[2] + ch[3])
            dat.append(ch[4] + ch[5])
            dat.append(ch[6] + ch[7])

        for data in dat:
            out_file.write(data)

        # Read the next block from file
        if encrypt:
            block = file.read(8)
        else:
            block = file.read(16)
