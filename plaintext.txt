==================================================
input is
plaintext:0123456789abcdef           NOTE: The input plaintext will be any ASCII text file
key:abcdef0123456789
All the subkeys are: 
----------------------------------------------
  57  37  78  12  79  7b  80  23  9b  bc   9  34
  ac  e2  d5  cd  cf  26  5e  de  f1  6a  e6  ef
   2  8d  2b  78  24  d1  b3  89  46  15  3c  9a
  79  7b  80  23  9b  bc   9  34  bd  c0  91  45
  cf  26  5e  de  f1  6a  e6  ef  13  af  6f  f0
  24  d1  b3  89  46  15  3c  9a  68  59  c4  ab
  9b  bc   9  34  bd  c0  91  45  de   4  1a  56
  f1  6a  e6  ef  13  af  6f  f0  35  f3  f7   1
  46  15  3c  9a  68  59  c4  ab  8a  9e  4d  bc
  bd  c0  91  45  de   4  1a  56  e0  48  a2  67
  13  af  6f  f0  35  f3  f7   1  57  37  78  12
  68  59  c4  ab  8a  9e  4d  bc  ac  e2  d5  cd
  de   4  1a  56  e0  48  a2  67   2  8d  2b  78
  35  f3  f7   1  57  37  78  12  79  7b  80  23
  8a  9e  4d  bc  ac  e2  d5  cd  cf  26  5e  de
  e0  48  a2  67   2  8d  2b  78  24  d1  b3  89
----------------------------------------------
After whiting: aaeeaa66aaeeaa66

This is round 0: 
g1:aa g2:ee g3:51 g4:8b g5:42 g6:de
g1:aa g2:66 g3:82 g4:ce g5:ad g6:a9
t0:42de, t1:ada9
f0:39ec, f1:3c99
after one round: 49816854aaeeaa66

This is round 1: 
g1:49 g2:81 g3:b3 g4:38 g5:15 g6:d4
g1:68 g2:54 g3:a1 g4:17 g5:15 g6:69
t0:15d4, t1:1569
f0:3210, f1:2800
after one round: 4c7f7ccd49816854

This is round 2: 
g1:4c g2:7f g3:b g4:b2 g5:7f g6:46
g1:7c g2:cd g3:4a g4:4 g5:71 g6:b9
t0:7f46, t1:71b9
f0:a8cd, f1:acdf
after one round: 70a67c774c7f7ccd

This is round 3: 
g1:70 g2:a6 g3:98 g4:18 g5:50 g6:da
g1:7c g2:77 g3:8f g4:cd g5:9b g6:8d
t0:50da, t1:9b8d
f0:45b4, f1:ce86
after one round: 84e5371c70a67c77

This is round 4: 
g1:84 g2:e5 g3:fe g4:9 g5:1e g6:a4
g1:37 g2:1c g3:91 g4:1d g5:90 g6:0
t0:1ea4, t1:9000
f0:5253, f1:3d38
after one round: 917ac5d684e5371c

This is round 5: 
g1:91 g2:7a g3:49 g4:b2 g5:9e g6:9c
g1:c5 g2:d6 g3:4c g4:8e g5:50 g6:ff
t0:9e9c, t1:50ff
f0:a8f3, f1:52e2
after one round: 160b3cda917ac5d6

This is round 6: 
g1:16 g2:b g3:9f g4:ab g5:fe g6:da
g1:3c g2:da g3:55 g4:1c g5:92 g6:bd
t0:feda, t1:92bd
f0:258, f1:aac7
after one round: 4991216a160b3cda

This is round 7: 
g1:49 g2:91 g3:7c g4:5b g5:42 g6:26
g1:21 g2:6a g3:ca g4:6c g5:49 g6:97
t0:4226, t1:4997
f0:b47, f1:c4e4
after one round: ea6bd504991216a

This is round 8: 
g1:e g2:a6 g3:6 g4:2c g5:e1 g6:c8
g1:bd g2:50 g3:c1 g4:98 g5:a7 g6:1
t0:e1c8, t1:a701
f0:ba68, f1:b84d
after one round: f9fcfa990ea6bd50

This is round 9: 
g1:f9 g2:fc g3:4f g4:e4 g5:b1 g6:b3
g1:fa g2:99 g3:e1 g4:d6 g5:1e g6:38
t0:b1b3, t1:1e38
f0:ce6b, f1:2405
after one round: e0665ea4f9fcfa99

This is round 10: 
g1:e0 g2:66 g3:1e g4:2d g5:65 g6:eb
g1:5e g2:a4 g3:95 g4:c1 g5:f g6:6e
t0:65eb, t1:f6e
f0:dbfe, f1:5356
after one round: 1101a665e0665ea4

This is round 11: 
g1:11 g2:1 g3:11 g4:ef g5:d2 g6:4
g1:a6 g2:65 g3:a g4:e8 g5:7 g6:43
t0:d204, t1:743
f0:8d6c, f1:8118
after one round: 36853c501101a665

This is round 12: 
g1:36 g2:85 g3:69 g4:1d g5:9d g6:63
g1:3c g2:50 g3:8 g4:69 g5:76 g6:44
t0:9d63, t1:7644
f0:8c78, f1:dc82
after one round: cebc904936853c50

This is round 13: 
g1:ce g2:bc g3:1c g4:10 g5:d9 g6:fc
g1:90 g2:49 g3:d4 g4:f7 g5:cc g6:e7
t0:d9fc, t1:cce7
f0:ed45, f1:102
after one round: 6de079a2cebc9049

This is round 14: 
g1:6d g2:e0 g3:f9 g4:89 g5:ed g6:30
g1:79 g2:a2 g3:d6 g4:50 g5:ea g6:38
t0:ed30, t1:ea38
f0:90c6, f1:2376
after one round: 2f3d03e56de079a2

This is round 15: 
g1:2f g2:3d g3:a9 g4:4a g5:9b g6:4f
g1:3 g2:e5 g3:c6 g4:f g5:d1 g6:8d
t0:9b4f, t1:d18d
f0:633a, f1:bbb4
after one round: 76d48f02f3d03e5

encrypted block: 84f0ece424282f79
==================================================
input is:
 ciphertext:84f0ece424282f79
 key:abcdef0123456789
All the subkeys are: 
----------------------------------------------
  e0  48  a2  67   2  8d  2b  78  24  d1  b3  89
  8a  9e  4d  bc  ac  e2  d5  cd  cf  26  5e  de
  35  f3  f7   1  57  37  78  12  79  7b  80  23
  de   4  1a  56  e0  48  a2  67   2  8d  2b  78
  68  59  c4  ab  8a  9e  4d  bc  ac  e2  d5  cd
  13  af  6f  f0  35  f3  f7   1  57  37  78  12
  bd  c0  91  45  de   4  1a  56  e0  48  a2  67
  46  15  3c  9a  68  59  c4  ab  8a  9e  4d  bc
  f1  6a  e6  ef  13  af  6f  f0  35  f3  f7   1
  9b  bc   9  34  bd  c0  91  45  de   4  1a  56
  24  d1  b3  89  46  15  3c  9a  68  59  c4  ab
  cf  26  5e  de  f1  6a  e6  ef  13  af  6f  f0
  79  7b  80  23  9b  bc   9  34  bd  c0  91  45
   2  8d  2b  78  24  d1  b3  89  46  15  3c  9a
  ac  e2  d5  cd  cf  26  5e  de  f1  6a  e6  ef
  57  37  78  12  79  7b  80  23  9b  bc   9  34
----------------------------------------------
After whiting: 2f3d03e5076d48f0

This is round 0: 
g1:2f g2:3d g3:a9 g4:4a g5:9b g6:4f
g1:3 g2:e5 g3:c6 g4:f g5:d1 g6:8d
t0:9b4f, t1:d18d
f0:633a, f1:bbb4
after one round: 6de079a22f3d03e5

This is round 1: 
g1:6d g2:e0 g3:f9 g4:89 g5:ed g6:30
g1:79 g2:a2 g3:d6 g4:50 g5:ea g6:38
t0:ed30, t1:ea38
f0:90c6, f1:2376
after one round: cebc90496de079a2

This is round 2: 
g1:ce g2:bc g3:1c g4:10 g5:d9 g6:fc
g1:90 g2:49 g3:d4 g4:f7 g5:cc g6:e7
t0:d9fc, t1:cce7
f0:ed45, f1:102
after one round: 36853c50cebc9049

This is round 3: 
g1:36 g2:85 g3:69 g4:1d g5:9d g6:63
g1:3c g2:50 g3:8 g4:69 g5:76 g6:44
t0:9d63, t1:7644
f0:8c78, f1:dc82
after one round: 1101a66536853c50

This is round 4: 
g1:11 g2:1 g3:11 g4:ef g5:d2 g6:4
g1:a6 g2:65 g3:a g4:e8 g5:7 g6:43
t0:d204, t1:743
f0:8d6c, f1:8118
after one round: e0665ea41101a665

This is round 5: 
g1:e0 g2:66 g3:1e g4:2d g5:65 g6:eb
g1:5e g2:a4 g3:95 g4:c1 g5:f g6:6e
t0:65eb, t1:f6e
f0:dbfe, f1:5356
after one round: f9fcfa99e0665ea4

This is round 6: 
g1:f9 g2:fc g3:4f g4:e4 g5:b1 g6:b3
g1:fa g2:99 g3:e1 g4:d6 g5:1e g6:38
t0:b1b3, t1:1e38
f0:ce6b, f1:2405
after one round: ea6bd50f9fcfa99

This is round 7: 
g1:e g2:a6 g3:6 g4:2c g5:e1 g6:c8
g1:bd g2:50 g3:c1 g4:98 g5:a7 g6:1
t0:e1c8, t1:a701
f0:ba68, f1:b84d
after one round: 4991216a0ea6bd50

This is round 8: 
g1:49 g2:91 g3:7c g4:5b g5:42 g6:26
g1:21 g2:6a g3:ca g4:6c g5:49 g6:97
t0:4226, t1:4997
f0:b47, f1:c4e4
after one round: 160b3cda4991216a

This is round 9: 
g1:16 g2:b g3:9f g4:ab g5:fe g6:da
g1:3c g2:da g3:55 g4:1c g5:92 g6:bd
t0:feda, t1:92bd
f0:258, f1:aac7
after one round: 917ac5d6160b3cda

This is round 10: 
g1:91 g2:7a g3:49 g4:b2 g5:9e g6:9c
g1:c5 g2:d6 g3:4c g4:8e g5:50 g6:ff
t0:9e9c, t1:50ff
f0:a8f3, f1:52e2
after one round: 84e5371c917ac5d6

This is round 11: 
g1:84 g2:e5 g3:fe g4:9 g5:1e g6:a4
g1:37 g2:1c g3:91 g4:1d g5:90 g6:0
t0:1ea4, t1:9000
f0:5253, f1:3d38
after one round: 70a67c7784e5371c

This is round 12: 
g1:70 g2:a6 g3:98 g4:18 g5:50 g6:da
g1:7c g2:77 g3:8f g4:cd g5:9b g6:8d
t0:50da, t1:9b8d
f0:45b4, f1:ce86
after one round: 4c7f7ccd70a67c77

This is round 13: 
g1:4c g2:7f g3:b g4:b2 g5:7f g6:46
g1:7c g2:cd g3:4a g4:4 g5:71 g6:b9
t0:7f46, t1:71b9
f0:a8cd, f1:acdf
after one round: 498168544c7f7ccd

This is round 14: 
g1:49 g2:81 g3:b3 g4:38 g5:15 g6:d4
g1:68 g2:54 g3:a1 g4:17 g5:15 g6:69
t0:15d4, t1:1569
f0:3210, f1:2800
after one round: aaeeaa6649816854

This is round 15: 
g1:aa g2:ee g3:51 g4:8b g5:42 g6:de
g1:aa g2:66 g3:82 g4:ce g5:ad g6:a9
t0:42de, t1:ada9
f0:39ec, f1:3c99
after one round: aaeeaa66aaeeaa66

decrypted block: 123456789abcdef